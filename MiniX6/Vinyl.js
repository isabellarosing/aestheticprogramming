class Vinyl {
   constructor()
   {
   this.speed = floor(random(3, 6));
   this.pos = new createVector(random(200,width-200),random(0,-200)); //random(width-80),0
   this.size = floor(random(15, 35));
   //rotate in clockwise for +ve no
   //this.vinyl_rotate = random(0, PI/20);
  //this.emoji_size = this.size/1.8;
   }
 move() {  //set the moving behaviors
   this.pos.y+=this.speed;  //i.e, this.pos.x = this.pos.x - this.speed;
 }
 show() { //show vinyl
   push()
   translate(this.pos.x, this.pos.y);
   //rotate(this.vinyl_rotate);
   image(vin, 0, this.size, 90, 90);
   pop();
}
}
