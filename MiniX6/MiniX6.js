/*based on ES6 (class-based object oriented programming is
introduced in ECMAScript 2015)
credit and inspiration:
game scene: ToFu Go by Francis Lam; emoji: Multi by David Reinfurt*/

let cartSize = {
  w:260,
  h:200
};
let cart;
let cartPosX;
let cartPosY;
let mini_height;
let min_vinyl = 5;  //minimun records on screen
let vinyl = [];
let score =0, lose = 0;
//let keyColor = 20;
let bg;

function preload(){
  bg = loadImage("data/bg.jpg"); //load background image
  cart = loadImage("data/cart.png"); //load png of shopping cart
  vin = loadImage("data/vin.png"); //load png of vinyl record
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  cartPosX = width/2.25; //initial placement of cart on x-axis
  cartPosY = height-300; //placement of cart on y-axis
  mini_height = width/2;
}
function draw() {
  background(bg); //set background to image
  displayScore();
  checkVinylNum(); //available vinyl
  showVinyl();
  image(cart, constrain(cartPosX,-100,width-200),cartPosY, cartSize.w, cartSize.h);
  checkCatching(); //scoring
  checkResult();

if (keyIsDown(LEFT_ARROW)){
  cartPosX -= 15;
}

if (keyIsDown(RIGHT_ARROW)){
  cartPosX += 15;
}
}

function checkVinylNum() {
  if (vinyl.length < min_vinyl) {
    vinyl.push(new Vinyl());
  }
}

function showVinyl(){
  for (let i = 0; i <vinyl.length; i++) {
    vinyl[i].move();
    vinyl[i].show();
  }
}

function checkCatching() {
  //calculate the distance between each record
  for (let i = 0; i < vinyl.length; i++) {
    let d = int(
      dist(cartPosX+cartSize.h/2,cartPosY+cartSize.w/2,
        vinyl[i].pos.x, vinyl[i].pos.y)
      );
    if (d < cartSize.h/2) { //close enough to catch the records
      score++;
      vinyl.splice(i,1);
    }else if (vinyl[i].pos.y > height) { //cart missed the records
      lose++;
      vinyl.splice(i,1);
    }
  }
}

function displayScore() {
    fill(200,0,0);
    textSize(17); //set text color red
    text('You have caught '+ score + " record(s)", 10, height/1.4);
    text('You have dropped ' + lose + " record(s)", 10, height/1.4+20);
    fill(1); //set text color black
    text('PRESS the ARROW LEFT & RIGHT key to catch the records',
    10, height/1.4+40);
}

function checkResult() {
  if (lose > 3) {
    fill(keyColor, 255);
    textSize(26);
    text("YOU DROPPED TOO MANY...GET OUT OF THE STORE", width/3, height/1.4);
    noLoop();
  } //lose when 3 records are dropped
}
