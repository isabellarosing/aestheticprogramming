MiniX6 - Games with objects
======

[View my project](https://isabellarosing.gitlab.io/aestheticprogramming/MiniX6/)

[View my code](https://gitlab.com/isabellarosing/aestheticprogramming/-/blob/main/MiniX6/MiniX6.js)

The program explained
------ 
**Describe how does/do your game/game objects work? Describe how you program the objects and their related attributes, and the methods in your game.**

For this week’s miniX the task was to make a game using object-oriented programming. My game is a record store game, where the player will have to catch records in order to not lose the game. It was originally based on the code from the Tofu game from the Aesthetic Programming Handbook (Soon & Cox, 2020), and I have then modified game to match my vision. When seeing this type of game, I thought of the computer games I played as a child, which was often older types of games. I then knew that I wanted to do something with a retro and nostalgic feel, centered around music. I made player character a black plastic crate, which were to catch the vinyl records as they fell from the sky towards to floor. If the player drops more than 3 records `if (lose > 3)`, the game is over.

I created the vinyl record as a `Class`, so I would have a “template” for all the objects, that would be falling down from the sky. In here I have a constructor as well as information about the size and speed of the object. This makes it much easier to program multiple records on screen for the game. I could probably also have made the player a Class to store information inside a separate file, but I didn’t find it necessary. A feature I enjoy, that makes the game run more smoothly is the `if (keyIsDown(RIGHT_ARROW))` statements.  

![Alt Text](https://gitlab.com/isabellarosing/aestheticprogramming/-/raw/main/MiniX6/MiniX6.gif)

**Draw upon the assigned reading, what are the characteristics of object-oriented programming and the wider implications of abstraction?**

>*”Abstraction is one of the key concepts of “Object-Oriented Programming” (OOP), a paradigm of programming in which programs are organized around data, or objects, rather than functions and logic. The main goal is to handle an object’s complexity by abstracting certain details and presenting a concrete model,”* (Soon & Cox, 2020)

Abstraction is, as seen in the quote above, an essential part of OOP. It is when we take complex objects from the real world and try to translate it to code, which is readable for the computer, and we can then use when programming. It is important to remember, that *”in doing so, certain details and contextual information are inevitably left out,”* (Soon & Cox, 2020).

When writing the code for my game, I didn’t actively think a whole lot about abstraction, I mostly thought of the things I wanted my objects to do. This is obviously also what abstraction is about, as I inevitably had to leave out some things to make room for my most important attributes. Originally, I wanted to have CD’s falling with more of a Y2K look, so that might emphasize the action of deliberately leaving things out.

**Connect your game project to a wider cultural context, and think of an example to describe how complex details and operations are being “abstracted”?**

Firstly, I don’t think I have chosen very complex objects, so they might be a little difficult to put into a wider cultural context. The game objects are operating very different from how they would naturally. I have left out sound, which could be quite relevant for a vinyl shop. The records are made two-dimensional and all of them are falling the same way, which would be unlikely in real life. I would say that the cultural context of my game is very much rooted in the “retro” and nostalgic feel it was based on. In my experience, living through a pandemic has seen people going back to their roots and more familiar, nostalgic hobbies and living spaces. 

References
------

[**P5.js website**](https://p5js.org/reference/)

Soon Winnie & Cox, Geoff, ["Object abstraction"](https://aesthetic-programming.net/pages/6-object-abstraction.html), Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 143-164

Matthew Fuller and Andrew Goffey, “The Obscure Objects of Object Orientation,” in Matthew Fuller, How to be a Geek: Essays on the Culture of Software (Cambridge:Polity, 2017).
