let x = 0;
let y = 0;

function setup() {
  createCanvas(windowWidth, windowHeight); //fit canvas to screen
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight); //make canvas instant-resize to window size
}

function draw() {
  background(245,227,204,80); //set background color, transparancy alpha value
  translate(width/2,height/2); //move 0,0 to center of screen
  frameRate(50); //set frameRate for draw function

  //loading leaves (appears when stem reaches them)
    fill(144,198,112); //set leaf color green
    noStroke();
  if(frameCount > 42){
    ellipse(22,120,40,15) //1st right leaf
  }

  if(frameCount > 99){
    ellipse(-22,105,40,15); //2nd left leaf
  }

  if(frameCount > 146){
    ellipse(22,90,40,15) //3rd right leaf
  }

  if(frameCount > 203){
    ellipse(-22,75,40,15); //4th left leaf
  }

  if(frameCount > 250){
    ellipse(22,60,40,15) //5th right leaf
  }

  if(frameCount > 307){
    ellipse(-22,45,40,15) //6th left leaf
  }

  //stem (moving upwards)
  for(let i=0; i < 35; i++){
    stroke(126,175,97); //set stem color green
    strokeWeight(6); //set stem thickness
    line(x,135,x,135+frameCount*(-0.3)); //make a line move upwards
  //reset line when frameCount reaches 425 to make it loop
  if(frameCount >= 425){
    frameCount = 0
    }
  }

  //make spinning flower appear (when flower middle appears)
  if(frameCount > 410){
    drawFlower();
  }

  //flower middle (appears when stem has reached the top)
  if(frameCount > 410){
    fill(79,67,39); //fill flower middle color brown
    noStroke();
    ellipse (x,y,20,20);
  }

    //soil
    noStroke()
    fill(79,67,39); //fill soil color brown
    bezier(-100,180,-10,80,100,180,100,180); //bezier to shape soil pile middle
    bezier(-150,180,-80,110,-50,180,-50,180); //bezier to shape soil pile left
    bezier(60,180,90,120,150,180,140,180); //bezier to shape soil pile right

  //text
  let s = 'nature loading... please be patient';
    fill(79,67,39);
    textSize(20);
    text(s,x-155,185,500,500);
}

/*The following code is made by modifying the spinning circles from the Class 03
lecture, section 3.3. The link for the executed code is https://bit.ly/3Id4r3t */
function drawFlower() {
  let num = 15; //make 15 petals
    push();
    frameRate(15); //set frameRate of the flower seperately from the rest
  // 360/num >> degree of each ellipse's movement;
  //frameCount%num >> get the remainder that to know which one
  let cir = 360/num*(frameCount%num);
    rotate(radians(cir));
    fill(241,194,50); //set flower color yellow
    noStroke();
    ellipse(20,0,35,20);
    pop();
}
