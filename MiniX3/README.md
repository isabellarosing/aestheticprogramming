MiniX3 - Designing a throbber
======

[View my project](https://isabellarosing.gitlab.io/aestheticprogramming/MiniX3/)

[View my code](https://gitlab.com/isabellarosing/aestheticprogramming/-/blob/main/MiniX3/MiniX3.js)


The program explained
------ 
**Describe your throbber design, both conceptually and technically. What do you want to explore and/or express?**

I have made a throbber, which resembles a plant stem growing from a pile of soil, gaining more leaves as it grows and blossoming a yellow flower, before it resets to repeat this process. Underneath the words *"nature loading... please be patient"* are written. 

I wanted to emulate how the natural cycle of time affects nature; a flower grows, it lives, it dies, it decomposes and then the cycle repeats itself. One could call it the *circle of life*, which is mirrored in the circular movement of the flower petals. This circular shape also parallels a standard clock, which is the most evident manifestation of time for me personally and as Lammerant mentions, has been a way for standardization of time measurement (pp. 90).

![Alt Text](https://gitlab.com/isabellarosing/aestheticprogramming/-/raw/main/MiniX3/MiniX3.gif)

The coding experience
------
**What are the time-related syntaxes/functions that you have used in your program, and why have you used them in this way? How is time being constructed in computation (refer to both the reading materials and your coding)?**

Firstly I want to mention, that I was very inspired by the [circulating ellipses](https://aesthetic-programming.gitlab.io/book/p5_SampleCode/ch3_InfiniteLoops/sketch3_1/), so I borrowed some of the code and modified it to make a floral design pattern. I used `frameRate` to lower the speed at which the frames are drawn inside the draw function. I have changed the frames to be 50 instead of the usual 60 frames per second, which is how my program knows how to slow down the movement of the throbber. I used a for-loop to make the plant stem grow upwards with each frame, but the main time-related syntax I used, was `frameCount`, which was also central to making the stem grow. I have also used `frameCount` to make the leaves, as well as the flower head, appear, when the plant stem reaches their position. 

In my throbber, you might say, that time is constructed by this growing of the flower and accentuated by the clock-like movement of the flower. This is what *”makes time move”* in my program. Generally, time as we humans experience it, is a construct. This is emphasized in Lammerant’s text, where he points out the way time used to be perceived, with nature and human routines at the center;

> ”Early time measurement was linked to observation of natural conditions like sunrise and sunset. Measurement of such sun cycles (which is in fact an earth cycle) through sundials allowed for more precise time referencing, but it is very place and season dependent,” (Lammerant, 2018, pp. 89).

which is very different to the standardized time we have now;

>”When humans became aware of earth as a sphere, they responded by flattening and linearising time. Mechanical clocks allowed for the unification of time lengths and thereby also standardized time, […] Mechanical clocks also allowed for the unification of different time cycles and scales and for the standardization of time over longer distances, departing from the local sundial time to time zones, which were less strictly linked to the seasonal rhythm of the sun and more to geographical zones,” (Lammerant, 2018, pp. 90).

My throbber symbolizes the way nature used to determine time and the cyclical process linked to the course of nature. This contrasts the fact, that my throbber was made with code on a computer, which is all a part of a standardized mechanical system. Even the program, in which the code was written, was built on this system, where eg. the draw function creates a new frame 60 times per second.


Personal experience with throbbers
------
**Think about a throbber that you have encounted in digital culture, e.g. for streaming video on YouTube or loading the latest feeds on Facebook, or waiting for a payment transaction, and consider what a throbber communicates, and/or hides? How might we characterize this icon differently?**

When thinking of throbbers, I have encountered myself, the first thing, that comes to mind is the so-called “spinning wheel/ball of death” or the spinning pinwheel in rainbow colors, that can appear, when a Mac computer is processing. When this happens, the pinwheel replaces the mouse and thereby stops the user from making any more actions with the mouse. The goal of this spinning wheel is to visually indicate to the user, that a process is in action, which wouldn’t necessarily be fathomable otherwise. It also temporarily stops the user from using the computer and overload it further, as long as the icon is shown. This could in some cases be interpreted as a deprivation of the user’s freedom to use their computer or could at least become very frustrating to endure.

Another example that comes to mind, when thinking about throbbers, is from an earlier version of YouTube. Back then, when a video was loading, a spinning wheel of circles would appear (not too far from the circulating ellipses mentioned earlier). If you knew the tricks to make this work, with a few clicks on the arrow keys, you could activate a game of *Snake* on the loading screen. In this instance, the throbber becomes a game to distract the viewer of the YouTube video. This might be interpreted as anything from a digital work of art, to completely insignificant for the viewers who aren’t aware of this function. 

Both of these examples substantiate and underline the quote below, that throbbers have no indication of progress. This can indeed make the users experience of throbbers pesky, which is why a thobber might be better suited for short-term waiting processes, while a progress bar might be better suited for longer waiting processes.
>"There is no indication of progress or status as is the case with a progress bar, for instance. We see the icon spinning, but it explains little about what goes on in the background or about timespan," (Soon & Cox).

With a glimpse into my personal experience with throbbers, I would claim, that a throbber won’t necessarily make a user less frustrated, or make a loading process better, but it can aim to make It more tolerable and maybe even entertaining. 


References
------

[**P5.js website**](https://p5js.org/reference/)

[**Circulating ellipses sample code**](https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2022/class03#33-sample-code)

Soon Winnie & Cox, Geoff, ["Infinite Loops"](https://aesthetic-programming.net/pages/3-infinite-loops.html), Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 71-96

Hans Lammerant, [“How humans and machines negotiate experience of time”](https://monoskop.org/log/?p=20190), in The Techno-Galactic Guide to Software Observation, 88-98, (2018).
