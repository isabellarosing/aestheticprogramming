MiniX5 - A generative program
======

[View my project](https://isabellarosing.gitlab.io/aestheticprogramming/MiniX5/)

[View my code](https://gitlab.com/isabellarosing/aestheticprogramming/-/blob/main/MiniX5/MiniX5.js)

The program explained
------ 
**What are the rules in your generative program? Describe how your program performs over time? How do the rules produce emergent behavior?**

I struggled quite a bit this week to make a generative program that would work as I imagined, within the frames of the assignment. This led me to make three different designs, all of which I just couldn’t get quite right. I had ideas and attributes, I couldn’t translate from my brain to functional code, so in the end I’ve had to accept a less appealing and interesting program. I ended up with the program seen below.


![Alt Text](https://gitlab.com/isabellarosing/aestheticprogramming/-/raw/main/MiniX5/MiniX5.gif)


I have made a noninteractive program to auto-generate sun and moon shapes randomly. 

**The rules** I set for my program were as follows: 

1.	The two shapes will appear randomly (there is a higher chance of a sun appearing) at a random position.
2.	The colors will change randomly between four possible hues for each of the two shapes.

The syntax `if (random(1) < 0.8) {` auto-generates numbers from 0 and 1. This means that if the number is under 0.8, a sun is drawn, and if it is over 0.8, a moon is drawn. This enables the program to randomly draw suns and moons depending on this auto-generated number. By using `let xpos = random(width)` and `let ypos = random(height)` as the x and y coordinates for the sun and moon, I was easily able to randomize the position of the shapes with every frame. I had to lower the `framerate` to make the viewing experience more comfortable. I have found, that using arrays to store and execute color-related syntax, is a really pleasant way to work with (randomizing) colors. Furthermore I have used the `push()``pop()` syntax to encapsule the information linked to each of the shapes, as to not “disturb” the rest of the code.



*(I did have a third and fourth rule, that I would’ve liked to implement, but wasn’t able to in time. 3. I would’ve preferred to make the program reset at a certain point, so the screen wouldn’t be completely covered in the sun and moon shapes. 4. I also really wanted the background to change as a condition of the visible sun-to-moon-ratio; a light blue shade when the suns are the majority, and a dark blue shade when the moons are the majority).* 


**What role do rules and processes have in your work?**

Rules were the boundaries for this miniX, and it was clearly very challenging for me to design an auto-generative program set within these boundaries. I think that these rules have a potential to make the coding process easier, as it can help kick off the ideation - having boundaries can certainly be a positive thing. Unfortunately, I found, for this project at least, that it really killed/limited my creativity and gave me extra obstacles along the way. I imagine that this poor encounter with set boundaries in the coding process, won’t be my general experience. It is likely just due to the frustration that appeared, when I came up with a really cool design, but wasn’t able to use it, as I couldn’t advocate the rules within the code.


**Draw upon the assigned reading, how does this MiniX help you to understand the idea of “auto-generator” (e.g. levels of control, autonomy, love and care via rules)? Do you have any further thoughts on the theme of this chapter?**

This miniX has let me explore auto-generative coding and has given me some wonderful learnings along the way. Not only have I gotten to know my own workstyle better, but I also feel like I have gained a better understanding of how to make certain syntax work in my program. I want to highlight the following quote as I feel it encompasses both my frustration and fascination with this miniX coding process. 

>”The computer is a unique device for the arts since it can function solely as an obedient tool with vast capabilities for controlling complicated and involved processes, but then again, full exploitation of its unique talents for controlled randomness and detailed algorithms could result in an entirely new medium—a creative artistic medium,” (Montfort, 2012, pp. 142)

One of my struggles during this process was probably in relation to this controlled randomness, which I had a hard time using for my benefit. I do see though, how auto-generation is a nice way of creating art and it was interesting to exclude interactivity in this miniX, as it gave an emphasis to the auto-generative and randomness needed for an artwork like this. I still think randomness can be a slightly challenging topic to get into, but I also think, that the concept of randomness I really interesting. I find it almost poetic or symbiotic that computers try to emulate humans by using randomness, but at the same time *”In some cases the computer has to turn to a human to become more random, recording data from users mashing the keys on their keyboard or wiggling their mouse around to generate a random key or password,”* (Montfort, 2012, pp. 141). 



References
------

[**P5.js website**](https://p5js.org/reference/)

Soon Winnie & Cox, Geoff, ["Auto-generator"](https://aesthetic-programming.net/pages/5-auto-generator.html), Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 121-142

Nick Montfort et al. [“Randomness”](https://10print.org/), 10 PRINT CHR$(205.5+RND(1)); : GOTO10, https://10print.org/ (Cambridge, MA: MIT Press, 2012), 119-146.
