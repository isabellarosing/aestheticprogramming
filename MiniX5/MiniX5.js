let sunC = ["#F4C430", "#F2C649", "#FFD700", "#FADA5E"]; //set color options for sun
let moonC = ["#E5E4E2", "#CCCCCC", "#B3B3B3","#CFCFC4"];  //set color options for moon
let skyNight = ["#334076"]; //set color for night
//let skyDay = ["#779ECB"];//set color for day


function setup() {
  createCanvas(windowWidth, windowHeight);
  background(skyNight); //set background color night
  //background(skyDay); //set background color day
  frameRate(10); //lower frameRate
}

function draw() {
  let xpos = random(width);
  let ypos = random(height);

/*auto-generates numbers from 0 and 1. if the number is under 0.8, a sun is
drawn, and if it is over 0.8, a moon is drawn*/
  if (random(1) < 0.8) {
    //sun
    push();
    strokeWeight(2); //set strokeWeight to 2
    stroke(248, 172, 29); //set stroke color orange
    fill(random(sunC)); //fill with randomized color options
    star(xpos, ypos, 30, 20, 25); //randomized x and y position
    pop();
  } else {
    //moon
    push();
    strokeWeight(2); //set strokeWeight to 2
    stroke(152, 152, 152); //set stroke color gray
    fill(random(moonC)); //fill with randomized color options
    ellipse (xpos, ypos, 40, 40); //randomized x and y position
    pop();
  }
}


//make star shape for the sun. Borrowed from https://p5js.org/examples/form-star.html
function star(x, y, radius1, radius2, npoints) {
  let angle = TWO_PI / npoints;
  let halfAngle = angle / 2.0;
  beginShape();
  for (let a = 0; a < TWO_PI; a += angle) {
    let sx = x + cos(a) * radius2;
    let sy = y + sin(a) * radius2;
    vertex(sx, sy);
    sx = x + cos(a + halfAngle) * radius1;
    sy = y + sin(a + halfAngle) * radius1;
    vertex(sx, sy);
  }
  endShape(CLOSE);''
}
