# Aesthetic Programming

### --- Individual assignments ---

##### ✨ MiniX1 ✨ [RunME 💌](https://isabellarosing.gitlab.io/aestheticprogramming/MiniX1/)  [Source code](https://gitlab.com/isabellarosing/aestheticprogramming/-/blob/main/MiniX1/MiniX1.js)

##### ✨ MiniX2 ✨ [RunME :anchor:](https://isabellarosing.gitlab.io/aestheticprogramming/MiniX2/)  [Source code](https://gitlab.com/isabellarosing/aestheticprogramming/-/blob/main/MiniX2/MiniX2.js)


##### ✨ MiniX3 ✨ [RunME 🌻](https://isabellarosing.gitlab.io/aestheticprogramming/MiniX3/)  [Source code](https://gitlab.com/isabellarosing/aestheticprogramming/-/blob/main/MiniX3/MiniX3.js)


##### ✨ MiniX4 ✨ [RunME :pill:](https://isabellarosing.gitlab.io/aestheticprogramming/MiniX4/)  [Source code](https://gitlab.com/isabellarosing/aestheticprogramming/-/blob/main/MiniX4/MiniX4.js)


##### ✨ MiniX5 ✨ [RunME :sun_with_face:](https://isabellarosing.gitlab.io/aestheticprogramming/MiniX5/)  [Source code ](https://gitlab.com/isabellarosing/aestheticprogramming/-/blob/main/MiniX5/MiniX5.js)


##### ✨ MiniX6 ✨ [RunME 💿](https://isabellarosing.gitlab.io/aestheticprogramming/MiniX6/)  [Source code](https://gitlab.com/isabellarosing/aestheticprogramming/-/blob/main/MiniX6/MiniX6.js)


##### ✨ MiniX7 ✨ [RunME 🎶](https://isabellarosing.gitlab.io/aestheticprogramming/MiniX7/)  [Source code](https://gitlab.com/isabellarosing/aestheticprogramming/-/blob/main/MiniX7/MiniX7.js)


### ----- Group assignments -----
Group 4: Maj, Mathilda, Trine, Isabella

##### ✨ MiniX8 ✨ [RunME :newspaper:](https://isabellarosing.gitlab.io/aestheticprogramming/MiniX8/)  [Source code](https://gitlab.com/isabellarosing/aestheticprogramming/-/blob/main/MiniX8/while_we_slept.js)
