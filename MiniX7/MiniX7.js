let cartSize = {
  w:320,
  h:250
};
let cart;
let cartPosX;
let cartPosY;
let vinyl = [];
let min_vinyl = 5; //minimun records on screen
let score =0, lose = 0;
let mode = 0;
let bg; //background image
let gameover; //end screen
let myFont; //font for score-counter
let song; //background music
let catchS; //sound effect when catching record
let button;

function preload(){
  song = loadSound("data/bgmusic.mp3"); //background music
  catchS = loadSound("data/catchS.wav"); //catch sound
  myFont = loadFont("data/Krungthep.ttf") //font used
  bg = loadImage("data/bg.jpg"); //background image
  start = loadImage("data/start.png"); //start screen
  gameover = loadImage("data/gameover.png"); //game-over screen
  cart = loadImage("data/cart.png"); //shopping cart
  vin = loadImage("data/vin.png"); //vinyl record
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  song.setVolume(0.4); //background music volume at 50%
  song.loop(); //loop music when finished
  cartPosX = width/2.25; //initial placement of cart on x-axis
  cartPosY = height-290; //placement of cart on y-axis
}

function startScreen(){
  background(bg); //background image for start screen
  image(start, width/4.85, height/2.8); //start screen image
}

function keyPressed() {
  if(keyCode === ENTER){ //press enter to start game
    mode = 1
  }
}

function draw() {
  if(mode == 0){ //display start screen
    startScreen()
  }else if(mode == 1){ //game starts when enter is pressed
    background(bg); //set background to image
    //draw box as background for displayScore
    fill("#211F2D");
    rect(22, height/1.08, 340, 54, 20, 20, 20, 20);
    displayScore();
    checkVinylNum();
    showVinyl();
    checkCatching();
    checkResult();
    image(cart, constrain(cartPosX, -90, width-250), cartPosY, cartSize.w, cartSize.h);
    //make cart move when arrows are pressed
    if (keyIsDown(LEFT_ARROW)){
      cartPosX -= 15;
    }
    if (keyIsDown(RIGHT_ARROW)){
      cartPosX += 15;
    }
  }
}

function checkVinylNum() {
  if (vinyl.length < min_vinyl) {
    vinyl.push(new Vinyl());
  }
}

function showVinyl(){
  for (let i = 0; i <vinyl.length; i++) {
    vinyl[i].move();
    vinyl[i].show();
  }
}

function checkCatching() {
  //calculate the distance between each record
  for (let i = 0; i < vinyl.length; i++) {
    let d = int(
      dist(cartPosX+cartSize.h/2, cartPosY+cartSize.w/2,
        vinyl[i].pos.x, vinyl[i].pos.y)
      );
    if (d < cartSize.h/2) { //close enough to catch the records
      score++;
      vinyl.splice(i,1); //removes record when caught
      catchS.play();
    }else if (vinyl[i].pos.y > height-50) { //cart missed the records
      lose++;
      vinyl.splice(i,1);
    }
  }
}

function displayScore() {
  fill("#F2EE6F");
  textFont(myFont);
  textSize(20);
  text('You have caught ' + score + " record(s)", 30, height/1.05);
  text('You have dropped ' + lose + " record(s)", 30, height/1.05+20);
}

function checkResult() {
  if (lose > 3) { //lose when more than 3 records are dropped
    image(gameover, width/4.1, height/3); //game over screen
    noLoop(); //stop game when game is lost
  }
}

/* NOTES - REFERENCES
Background music: Available on Audiio.com
Sound effect: Available on zapsplat.com
Font: Available from Apple/MacOS
Background image: Available on unsplash.com
Start screen: Made with Adobe Illustrator
Game over screen: Made with Adobe Illustrator
Cart/crate: Available on picsart.com
Vinyl record: Available on pngimg.com
*/
