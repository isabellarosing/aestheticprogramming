MiniX7 - Revisit the past
======

[View my project](https://isabellarosing.gitlab.io/aestheticprogramming/MiniX7/)

[View my code](https://gitlab.com/isabellarosing/aestheticprogramming/-/blob/main/MiniX7/MiniX7.js)

**NOTE:** Remember to give access to media with sound *Autoplay* in your browser preferences.

----
**Which miniX have you reworked?**

![Alt Text](https://gitlab.com/isabellarosing/aestheticprogramming/-/raw/main/MiniX7/MiniX7.gif)

For this week’s miniX the task was to revisit and rework one of our earlier miniX assignments. I decided that I wanted to revisit my previous miniX6. This decision was made on the grounds of not being completely satisfied with the first edition of my game, and because I felt that I had learnt a few things to make some improvements to the game. I started my process off by figuring out some ideas for improvements, which I have listed below. I wanted to add:

-	Music and sound effect
-	A start screen, which would show the instructions and start the game with the press of a key
-	A game over screen
-	Additional adjustments to make the game run more smoothly and look more pleasing

This leads me to the next writing prompt

**What have you changed and why?**

I have made visual adjustments to the game to upgrade the look and overall experience for the player. The score counter has gotten a custom font, a `rect` for its background to make it easier to read, and color to match the vinyl records. I have increased the size of the records and the cart to match the sleeve sizes on the background image better. This has an added bonus of making the game easier to play, because the cart has to travel shorter on the x-axis. I was able to remove the instructions from the game screen, by adding a start screen, where they are displayed. Lastly, I have added a game over screen, which is shown, when the game is lost. 

I have made functional adjustments to improve the gameplay, by adjusting the points, where the records are caught by the cart. I would like to highlight the improvements implemented by the start screen especially. The benefits of adding a start screen are not only visual, but also gave me the possibility to make a *press enter to start* option with `keyPressed`. This gives the player a chance to navigate the game rules before they play, because the game only starts as soon as they press enter. This is especially useful for people who haven’t tried this type of game before (which to be fair probably isn’t many).

Additionally, I have given the game some background music, and a record scratch sound effect when records are caught. Initially I realized, that having a game that centered around music without any sound to it, made it feel empty. In my opinion the added music gives the right ambience to the game, and the sound effect gives the player audio-feedback during the game, which I believe makes it more engaging to play. 

**What have you learnt in this mini X? How did you incorporate/advance/interprete the concepts in your ReadMe/RunMe (the relation to the assigned readings)?**

The changes and adjustments I have made from the original miniX, has given the game an overall more cohesive feel and look. I feel like this miniX has taught me many useful tools to use for mt future coding. This is my first time adding things like custom fonts and sounds to my code, which has been a great learning experience. I struggled a lot with getting the sound to work on my browser, even when the code seemed as it should be working. This carried on for a while until I learned that my browser was blocking the sound coming from my game. As soon as I fixed that problem, my game ran nicely with the intended music and sound effects, so sometimes a problem won’t be located in the code but in an external and unexpected source.


**What is the relation between aesthetic programming and digital culture? How does your work demonstrate the perspective of aesthetic programming (look at the Preface of the Aesthetic Programming book)?**

As digitalization is increasingly prevalent in many cultures, these platforms are becoming an essential part of society. Whether it is through social media, streaming services, remote online work or so on, digital culture is incredibly important for many people’s everyday lives. This is where programming and especially aesthetic programming comes into the picture.

My work is slightly paradoxical, as it is a completely digital product modeled after something analogue, where vinyl discs are pressed to be able to play songs, we could play from our phones in seconds. It illustrates a duality in digital culture, because of an appreciation of “outdated” technology, while having a focus on developing the newest technology constantly. Lastly, this course in its entirety demonstrates the ability of people learning to code in quite a short amount of time. I think an interesting quote is presented in Anette Vee’s article: *“Someday computer literacy will be a condition for employment, possibly for survival, because the computer illiterate will be cut off from most sources of information,”* (Vee, 2017, pp. 43).
While I don’t necessarily agree with the statements put forward in this article, that being able to code is comparable to mass literacy or is as important, I do think that coding is very useful in various circumstances. It gives an overall better understanding of how things around us in digital culture function. I personally think the Aesthetic Programming Handbook gives a better justification of programming and culture in the following quote: 

>*“We therefore consider programming to be a dynamic cultural practice and phenomenon, a way of thinking and doing in the world, and a means of understanding some of the complex procedures that underwrite and constitute our lived realities, in order to act upon those realities.”* (Soon & Cox, 2020).


References
------

[**P5.js website**](https://p5js.org/reference/)

Soon Winnie & Cox, Geoff, ["Preface"](https://aesthetic-programming.net/pages/6-object-abstraction.html), Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 13-24.

Vee, Anette, "Coding for Everyone and the Legacy of Mass Literacy", Coding Literacty: How Computer Programming Is Changing Writing Cambridge, Mass.: MIT Press, 2017), 43-93.
