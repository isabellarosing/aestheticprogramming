class Vinyl {
   constructor() {
     this.speed = floor(random(3, 6));
     this.pos = new createVector(random(200, width-200), random(0, -200)); //random(width-80),0
     this.size = floor(random(15, 35));
   }

   move() {  //set the moving behaviors
     this.pos.y+=this.speed;  //i.e, this.pos.x = this.pos.x - this.speed;
   }

   show() { //show vinyl
     push()
     translate(this.pos.x, this.pos.y);
     imageMode(CENTER);
     image(vin, 0, this.size, 130, 130);
     pop();
   }

}
