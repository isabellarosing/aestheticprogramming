function setup(){
  createCanvas(800,800); //set canvas size
}

function draw(){
  background(208,220,228); //set background color

  if (mouseIsPressed === true) {

    //ulo1
    fill(158,155,151); //blade edge - set color
    stroke(158,155,151); //blade edge - set stroke color to match fill color
    strokeWeight(1);
    curve(100,-250,190,490,610,490,700,-250); //blade edge - shape and placement

    fill(188,185,181); //blade - set color

    stroke(188,185,181); //blade body - set stroke color to match fill color
    strokeWeight(1);
    triangle(400,350,190,490,610,490); //blade body - top half placement and size
    curve(100,-80,190,490,610,490,700,-80); //blade body - bottom half placement and size

    stroke(158,155,151); //blade neck - set stroke color to match blade edge
    strokeWeight(20);
    line(400,310,400,440); //blade neck - placement and size

    noStroke(); //handle - noStroke to hide connecting geometric parts
    fill(242,236,226); //handle - set color
    ellipse(400,270,300,80); //handle - top placement and size
    rect(250,270,300,40,0,0,10,10); //handle - bottom placement and size

  } else {

    //ulo2
      fill(178,175,171); //blade edge - set color
      stroke(178,175,171); //blade edge - set stroke color to match fill color
      curve(100,-250,190,490,610,490,700,-250); //blade edge - shape and placement

      fill(198,195,191); //blade - set color

      noStroke(); //blade neck
      quad(380,246,420,246,440,410,360,410); //blade neck - placement and size

      stroke(198,195,191); //blade body - set stroke color to match fill color
      curve(100,1200,190,490,610,490,700,1200); //blade body - top half placement and size
      curve(100,-80,190,490,610,490,700,-80); //blade body - bottom half placement and size

      noStroke(); //handle - noStroke to hide connecting geometric parts
      fill(242,236,226); //handle - set color
      ellipse(400,270,300,50); //handle - top placement and size
      ellipse(400,300,300,50); //handle - bottom placement and size
      rect(250,270,300,30); //handle - middle placement and size
}

  print(mouseIsPressed);

}
