MiniX2 - Geometric emoji
======

[View my project](https://isabellarosing.gitlab.io/aestheticprogramming/MiniX2/)

[View my code](https://gitlab.com/isabellarosing/aestheticprogramming/-/blob/main/MiniX2/MiniX2.js)

------ 

For this week’s MiniX we were asked to design two emojis, made from geometric shapes, which we would be able to put into a wider cultural and social context. My first decision was that I did not want to make a regular smiley-face emoji and that I wanted to make something of personal significance to myself. I quickly found out that I wanted to take inspiration from my Greenlandic/Inuit roots, as it is something of big significance to me and a culture which I barely see represented in the current emoji selection. I started to explore what emojis my family members and I would like to see and be able to use in our everyday lives. I found many interesting and culturally important artefacts that I would potentially be able to make into an emoji, of which my first thought was to make an Ulo.

Ulo mini history
-----

The Ulo is an important tool to the Inuit, both practically and culturally. It is a so-called woman’s knife which is used to cut open an animal and carefully separate the skin from the meat, so the skin can be used for clothes and the like. It is a tool that has been used for many years, but it is still used by Inuit today [*(Reference 1)*](https://www.inanuukyorkminute.com/ulo-den-groenlandske-kvindekniv/) and the Ulo has developed different iterations varying on different locations of Greenland and the Arctic [*(Reference 2)*](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwimtKb7vo_2AhWBQvEDHVx8A0kQFnoECAQQAQ&url=http%3A%2F%2Fwww.dagensgronland.dk%2Ffile%2F313%2F121_KvindeknivenUloen.pdf&usg=AOvVaw36NdbvwTtMLfOoOLwrM8a6). Other than being a functional tool it is also a symbol of Inuit culture and probably something that can be found in many Greenlandic homes. 

The coding experience
------

When I began coding the Ulo I started out with the most basic shapes, that I had already gotten to know from last week's exercise. I mainly used ellipses and rectangles to sketch out my emoji. I was not exactly satisfied with the initial result though, so I ended up experimenting with layering geometric shapes as well as using curves to get the desired look. The handle is made up of ellipses and a rounded rectangle, the neck is made of a quad, and the blade is made from two mirrored curves to get the sharp edges. The last step was to select the right colors and add a shadow. 

I tried out different Inuit symbols and artefacts, such as a *Tupilak* and *Thulemanden* but I did not end up liking my interpretation and design of these enough to include it in my MiniX. Instead, I ended up making another Ulo design to go with my original one. This ulo, inspired by Canadian design, is made with a similar curved blade, but with a triangle for the top of the blade. The handle is made of a rounded rectangle and single ellipse. For the connecting piece I used a simple line and adjusted the strokeWeight to fit the desired look.

In my search for other Ulo designs, I discovered that the different regions have different types of designs, and that the one I knew growing up, was the West Greenlandic one, which is the area my family is from. I thought that this was very interesting as it highlights the differences and similarities there are when comparing the same tool over different arctic regions. 


The program explained
------ 

The program displays an Ulo of West Greenlandic resemblance. When the screen is pressed, another Ulo of Canadian resemblance appears but it is only visible if the mouse is pressed. For this I used an if-else statement, which dictates, that ulo2 will be displayed if the mouse is pressed, and if not ulo1 will be shown. It is quite a simple syntax, but it achieved what I needed, which was to display my two emojis separately. I wanted the changing Ulo’s to represent the similarities and differences of the varying arctic cultures. Also, if we imagine them actually being available to use, it would give the user different, more personal options, like we see with different color variations on current emojis.


![Alt Text](https://gitlab.com/isabellarosing/aestheticprogramming/-/raw/main/MiniX2/ulo.gif)


Examining the text “Modifying the Universal” I think it is interesting to look into representation of different cultures and identities in emojis. One could argue that representation is not nearly good enough, when many people are not able to find themselves represented in the standard emojis. 
I think that it is relevant to consider whether having more diverse options would be a way to fight discrimination or give the possibility of people abusing these emojis. Is it even possible to represent every culture, identity or job position? I personally feel like complete universality is far from attainable, but I also recognize the joy of seeing the Ulo’s I created, which are significant for my culture, and imagining that being a part of the standard emoji selection. That joy is something I would like for every person to experience when they see themselves represented in something as “insignificant” as an emoji. 


Closing thoughts
------

If I had had more time, I would have liked to program my emojis so they would switch between the two, when clicked. I tried multiple ways, but could not get it to work, so instead, with a little help from Trine, I made them change when pressed. This ended up being sufficient for what I needed, but I would like to figure out how to make the mouseClicked work for later.


References
------

[**P5.js website**](https://p5js.org/reference/)

**1** [Examples of the Ulo in other contexts](https://www.inanuukyorkminute.com/ulo-den-groenlandske-kvindekniv/)

**2** [Different iterations of the Ulo](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwimtKb7vo_2AhWBQvEDHVx8A0kQFnoECAQQAQ&url=http%3A%2F%2Fwww.dagensgronland.dk%2Ffile%2F313%2F121_KvindeknivenUloen.pdf&usg=AOvVaw36NdbvwTtMLfOoOLwrM8a6)

**3** [Roel Roscam Abbing, Peggy Pierrot and Femke Snelting, “Modifying the Universal,” in Helen Pritchard, Eric Snodgrass & Magda Tyżlik-Carver, eds., Executing Practices (London: Open Humanities Press, 2018), 35-51](http://www.data-browser.net/pdf/DB06_Executing_Practices.pdf)

