var y;
var speed;
var x;


function setup() {
  createCanvas(700, 400);
  y = 0; // Start at the center
  speed = 0.4;
}

function draw() {
  background(155,193,227);

  stroke(0)
  fill('pink')

      //envelope top 1
      triangle(25,320,105,320,65,280)

      //envelope top 3
      triangle(395,320,475,320,435,280)

    fill('white')

      //envelope top 2
      triangle(210,320,290,320,250,280)

      //envelope top 4
      triangle(580,320,660,320,620,280)

  for (x = 0; x < width + 100; x = x + 185) {

    stroke(0)
    fill('pink')

        //envelope bottom 1
        rect(25,320,80,50)

        //envelope bottom 3
        rect(395,320,80,50)

    fill('white')

        //envelope bottom 2
        rect(210,320,80,50)

        //envelope bottom 4
        rect(580,320,80,50)

//moving hearts
  noErase()
  fill('red');
  noStroke();
  ellipse(x+57, y+0, 20, 20)
  ellipse(x+73, y+0, 20, 20);
  triangle(x+81, y+6, x+65, y+28, x+49, y+6);
    y = y + speed;

    if (y > 330) { // If reached the bottom edge of the canvas, go back to the top edge
      y = 0;
    }
  }
}
