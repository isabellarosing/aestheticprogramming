MiniX1 - RunME and ReadME
======

[View my project](https://isabellarosing.gitlab.io/aestheticprogramming/MiniX1/)

[View my code](https://gitlab.com/isabellarosing/aestheticprogramming/-/blob/main/MiniX1/MiniX1.js)

------ 

For this first mini exercise, my main focus was to get acquainted with a few P5.js syntaxes and get more confident with working in Atom and Github. I tried making a few different sketches before getting the idea for this one. I knew I wanted to challenge myself and my lack of coding experience by making a moving image. 

I ended up producing a row of hearts, that are falling into open envelopes on a loop. My inspiration for the final sketch was the upcoming Valentine’s Day, and wanting to do something, that fit the theme. I worked with the following syntaxes: ellipse, rect, triangle, (no)fill, (no)stroke, (no)erase and a few others.


![Alt Text](https://gitlab.com/isabellarosing/aestheticprogramming/-/raw/main/MiniX1/MiniX1.gif)


Making a moving sketch came to be more of a challenge than first expected, and I did struggle a lot along the way to make the code work as intended. I managed to find an example of code that matched my vision [*(Reference 1)*](https://editor.p5js.org/cs4all/sketches/Hyv07I6nZ) of the falling hearts [*(Reference 2)*](https://p5js.org/reference/#/p5/describe), but figuring out how to combine it and was very time-consuming. I wanted to understand what the specific lines of code were executing, which is why I wrote the copied code by hand, and not just copy-pasting, to better understand it.

I found it fairly easy to make static sketches after exploring the [P5.js website](https://p5js.org/reference/) for a short time, and it quickly felt like writing “regular” text. I was challenged much more when it came to the moving sketches. I found the code more complicated because it had many variables to perfectly align before the sketch would work. I found myself having to almost randomly edit the code to see how it was affected, thereby making it more about trial and error, than writing “regular” text.

If I had had more time, I would have loved to do some cool additions to the sketch but considering the circumstances I am pretty content with this week’s work. I have gained a basic knowledge of P5.js in conjunction with this week’s literature and feel like I know how to navigate Atom and Github, which was my main goal. 


References
------

**1** [Moving downwards on a loop](https://editor.p5js.org/cs4all/sketches/Hyv07I6nZ)

**2** [Heart shape](https://p5js.org/reference/#/p5/describe)
