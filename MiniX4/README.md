MiniX4 - Capture all
======

[View my project](https://isabellarosing.gitlab.io/aestheticprogramming/MiniX4/)

[View my code](https://gitlab.com/isabellarosing/aestheticprogramming/-/blob/main/MiniX4/MiniX4.js)

The presentation
------ 
**Provide a title for and a short description of your work (1000 characters or less) as if you were going to submit it to the festival.**

:pill: **Red pill / blue pill** Is a critical work that means to emphasize the vulnerability of the population in regards to data-capture.

The program asks the player to choose between two colored pills:

You choose the **blue** pill, and the button takes you to a nice, quiet and safe space, where you will feel, you belong. You will live blissfully unaware of the surveillance happening of you when your data is being captured. As in the Matrix, you will still be utilized by outside forces capturing your data, but you won’t know a thing about it. 

If you choose the **red** pill on the other hand, you are unplugged from the “Matrix”, and you now get to see reality clearly for what it is. At which point the surveillance happening of you will be directly visible on the screen in front of you. You will be constantly aware of the data capture from this point on and the only option to stop the surveillance is to completely unplug from the system - by getting offline or maybe getting a VPN.

![Alt Text](https://gitlab.com/isabellarosing/aestheticprogramming/-/raw/main/MiniX4/MiniX4.png)

The program explained and coding experience
------
**Describe your program and what you have used and learnt.**

For this week, we were asked to experiment with data-capturing through different modes of input. At first in this process, I had no ideas of what to make for my program, but after a while I came up with the idea to make a red pill / blue pill situation, heavily inspired by the Matrix movies. I have created a program, where the user will be given a choice between the red and blue pill. Unlike the movie, they will not get any further explanation, before making their choice. This might mirror how many consumers aren’t aware or aren’t bothered to check when accepting cookies, browsing on public networks, and otherwise sharing data.

I have used `createButton` and stylized them with CSS to make my two pill shapes. I used the `mousePressed` function to call specific functions, when pressing the two buttons.
As briefly mentioned in the previous paragraph, when the blue button is chosen, an image and some text is loaded, to tell the player “still in Matrix”, that everything is fine. When the red button is chosen, the player is taken to a screen, that will use `createCapture(VIDEO)` to show input from the webcam and text is displayed over it, to welcome the player into a more aware state of being.


Closing thoughts
------
**Articulate how your program and thinking address the theme of “capture all.” What are the cultural implications of data capture?**

Datafication can be seen as both beneficial and as a breach of privacy. When I began thinking about data-capture and datafication, the first thing, that came to mind was the importance datafication has for the advertising industry. Within the last couple of years, I learned, that you can access an overview of all the information Google has stored on you, from your Google account. This is done to more efficiently advertise products or services to you, the consumer, and Google are likely not the only ones gathering large amounts of data to do this. The link between advertising and the Matrix might not be obvious, and I want to emphasize, that one might have to use exaggerated future scenarios for this to make sense, but I found the idea a connection fascinating.

As mentioned in the Tansmediale content call, we are subjected to having our data captured everywhere we go - this being especially, but far from exclusively on the internet. Companies have various options of capturing our data and some people might be more dispositioned to getting their information captured because of this. In reference to my program, this is quite an extreme, futuristic, sci-fi like idea of the cultural implications of datafication and capture all. I do however find this to be an interesting allegory of how different people’s approach to having their data captured is, and understanding digital capitalism and capture all can be essential to make sure ones data aren’t being misused.

References
------

[**P5.js website**](https://p5js.org/reference/)

Soon Winnie & Cox, Geoff, ["Data capture"](https://aesthetic-programming.net/pages/5-auto-generator.html), Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 97-119

Mejias, Ulises A. and Nick Couldry. "Datafication". Internet Policy Review 8.4 (2019). Web. 16 Feb. 2021.

[Transmediale Call for Works 2015](https://archive.transmediale.de/content/call-for-works-2015)
