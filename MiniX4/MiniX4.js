let buttonR; //make a variable for red button
let buttonB; //make a variable for blue button
let blueP;
let hands;
//let capture;
let videoCam;

function preload() {
  hands = loadImage("data/hands.jpg"); //credits to unsplash.com
  blueP = loadImage("data/bluep.png"); //credits to unsplash.com
}

function setup() {
  createCanvas(windowWidth, windowHeight); //fit size to window
  background("#131615"); //set background color
  image(hands, width/6, height/7, 1200, 700); //place image of hands

  //start text
  fill(0, 255, 65);
  textSize(60);
  text("Which one will you choose?", width/3.3, 50, 800, 500);

  //red button
  buttonR = createButton('Red pill'); //create red pill button
  buttonR.position(width/2.72, height/2.2); //set red pill position
  buttonR.style("border", "1.5px solid #A8271F"); //make thin border in red
  buttonR.style("border-radius", "50px"); //make corners round
  buttonR.style("padding", "15px 40px"); //add padding to determine pill size
  buttonR.style("background", "#CC342B"); //give button red color
  buttonR.style("font-weight", "bold"); //make font bold
  buttonR.style("font-size", "30px"); //make font size 30 px
  buttonR.mousePressed(chooseRed); //start function(chooseRed)

  //blue button
  buttonB = createButton('Blue pill'); //create blue pill button
  buttonB.position(width/1.88, height/2.2); //set blue pill position
  buttonB.style("border", "1.5px solid #263A99"); //make thin border in blue
  buttonB.style("border-radius", "50px"); //make corners round
  buttonB.style("padding", "15px 40px"); //add padding to determine pill size
  buttonB.style("background", "#2A42B1"); //give button blue color
  buttonB.style("font-weight", "bold"); //make font bold
  buttonB.style("font-size", "30px"); //make font size 30 px
  buttonB.mousePressed(chooseBlue); //start function(chooseBlue)

  //video
  videoCam = createCapture(VIDEO);
  videoCam.hide(); //hide video until called
}

function chooseRed() {
  buttonR.hide(); //hide button
  buttonB.hide(); //hide button
  background(108, 123, 127);
  fill(21, 31, 40);
  textSize(60);
  text("Welcome to the real world", width/3.3, 60, 800, 500);
  videoCam.position(windowWidth/4.5, windowHeight/6); //video placement
  videoCam.size(1000, 600); //video size
  videoCam.show(); //show video when this function is called
}

function chooseBlue() {
  background(121, 149, 110);
  fill(64, 79, 68);
  textSize(60);
  text("Enjoy your life in peace and quiet", width/3.8, 100, 1000, 500);
  image(blueP, width/4, height/5, 950, 600); //place image
  buttonR.hide(); //hide button
  buttonB.hide(); //hide button
  myText1.hide(); //hide text
  noLoop();
}
